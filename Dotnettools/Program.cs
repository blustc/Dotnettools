﻿using System;
using System.IO;
using System.Reflection;
using Dotnettools.beans;
using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;
using MR.AttributeDI.ServiceCollection;
using Newtonsoft.Json;

namespace Dotnettools
{
    class Program
    {
        private static IServiceProvider CreateProvider()
        {
            var services = new ServiceCollection();

            services.Configure(typeof(Program).GetTypeInfo().Assembly);

            return services.BuildServiceProvider();
        }

        static void Main(string[] args)
        {
            CommandLineApplication app = new CommandLineApplication();
            app.HelpOption("--help|-h|-?");
            app.VersionOption("--version|-v", "1.0.0");

            CommandOption optService = app.Option("--service -s <value>", "service", CommandOptionType.SingleValue);
            CommandOption optMessage = app.Option("--message -m <value>", "message", CommandOptionType.SingleValue);
            CommandOption optKey = app.Option("--key -k <value>", "key", CommandOptionType.SingleValue);
            CommandOption optValue = app.Option("--value -v <value>", "value", CommandOptionType.SingleValue);

            app.OnExecute(() => OnAppExecute(optService, optMessage, optKey, optValue));
            app.Execute(args);
            //System.Environment.Exit(-1);

        }

        private static int OnAppExecute(CommandOption optService, CommandOption optMessage, CommandOption optKey, CommandOption optValue)
        {
            var provider = CreateProvider();
            var service = optService.Value();
            switch (service)
            {
                case "rabbitmq_send":
                    using (var rabbitmq = provider.GetRequiredService<Rabbitmq>())
                    {
                        rabbitmq.sendMsg(optMessage.Value());
                    }
                    break;

                case "rabbitmq_receive":
                    using (var rabbitmq = provider.GetRequiredService<Rabbitmq>())
                    {
                        rabbitmq.startConsumer();
                    }
                    break;

                case "redis_send":
                    using (var redis = provider.GetRequiredService<Redis>())
                    {
                        redis.set(optKey.Value(), optValue.Value());
                    }
                    break;

                case "redis_get":
                    using (var redis = provider.GetRequiredService<Redis>())
                    {
                        var value = redis.get(optKey.Value());
                        Console.WriteLine("redis_get value: " + value);
                    }
                    break;

                case "insert_one":
                    using (var mongo = provider.GetRequiredService<Mongo>())
                    {
                        mongo.insert_one("default_db","test_1");

                    }
                    break;

                default:
                    Console.WriteLine("no service");
                    break;
            }
            return 0;
        }
    }
}