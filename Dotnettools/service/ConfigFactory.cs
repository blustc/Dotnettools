﻿using System.IO;
using Dotnettools.beans;
using Newtonsoft.Json;

namespace Dotnettools.service
{
    public class ConfigFactory
    {
        private static ConfigFactory _configConfig;

        private static readonly object padlock = new object();

        public Config config { get; }

        private ConfigFactory()
        {
            var configJson = File.ReadAllText("config.json");
            config = JsonConvert.DeserializeObject<Config>(configJson);
        }

        public static ConfigFactory Instance
        {
            get
            {
                lock (padlock)
                {
                    if (_configConfig == null)
                    {
                        _configConfig = new ConfigFactory();
                    }
                }

                return _configConfig;
            }
        }

    }
}