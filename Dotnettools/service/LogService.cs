﻿using MR.AttributeDI;
using Serilog;
using Serilog.Core;

namespace Dotnettools
{
    [AddToServices(Lifetime.Singleton)]
    public class LogService
    {
        private Logger _logger;

        public LogService()
        {
            _logger = new LoggerConfiguration()
                .MinimumLevel.Debug()//等级
                .WriteTo.LiterateConsole()//写到控制台
                .WriteTo.RollingFile("logs\\{Date}.txt")//写到文本
                .CreateLogger();
        }

        public void log(string message)
        {
            _logger.Information(message);
        }
    }
}