﻿using System;
using Dotnettools.service;
using MongoDB.Bson;
using MongoDB.Driver;
using MR.AttributeDI;

namespace Dotnettools
{
    [AddToServices(Lifetime.Singleton)]
    public class Mongo  : IDisposable
    {
        private MongoClient _client;
        public Mongo()
        {
            _client = new MongoClient(ConfigFactory.Instance.config.mongo.host_name);
        }

        public void insert_one(string db, string mongoCollection)
        {
            var database = _client.GetDatabase(db);
            var collection = database.GetCollection<BsonDocument>(mongoCollection);
            var document = new BsonDocument
            {
                { "name", "MongoDB" },
                { "type", "Database" },
                { "count", 1 },
                { "info", new BsonDocument
                {
                    { "x", 203 },
                    { "y", 102 }
                }}
            };
            collection.InsertOne(document);
        }

        public void Dispose()
        {

        }
    }
}