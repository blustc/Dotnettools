﻿using System;
using Dotnettools.service;
using MR.AttributeDI;
using StackExchange.Redis;

namespace Dotnettools
{
    [AddToServices(Lifetime.Singleton)]
    public class Redis : IDisposable
    {

        private ConnectionMultiplexer redis;
        private IDatabase db;

        public Redis()
        {
            redis = ConnectionMultiplexer.Connect(ConfigFactory.Instance.config.redis.host_name);
            db = redis.GetDatabase(ConfigFactory.Instance.config.redis.db_id);
        }

        public void set(string key, string value)
        {
            db.StringSet(key, value);
        }

        public string get(string key)
        {
            RedisValue rv = db.StringGet(key);
            return rv.ToString();
        }

        public void Dispose()
        {

        }
    }


}