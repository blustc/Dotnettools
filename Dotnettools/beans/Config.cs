﻿namespace Dotnettools.beans
{
    public class Config
    {
        public RabbitMqConfig rabbitmq { get; set; }
        public RedisConfig redis { get; set; }
        public MongoConfig mongo { get; set; }
        public MongoConfig mongo_cluster { get; set; }
    }

    public class BaseConfig
    {
        public string host_name  { get; set; }
        public int port { get; set; }
        public string user_name { get; set; }
        public string password { get; set; }
    }

    public class RabbitMqConfig : BaseConfig
    {

        public string virtual_host { get; set; }
        public string exchange_name { get; set; }
        public string queue_name { get; set; }
        public string router_key { get; set; }

    }

    public class RedisConfig : BaseConfig
    {
        public int db_id { get; set; }
    }

    public class MongoConfig : BaseConfig
    {
    }
}